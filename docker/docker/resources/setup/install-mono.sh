#!/usr/bin/env bash

set -e

# Install dependencies
apt-get update -y
apt-get install -y gnupg ca-certificates libicu60

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | tee /etc/apt/sources.list.d/mono-official-stable.list
apt-get update -y

apt-get install -y mono-devel ca-certificates-mono
