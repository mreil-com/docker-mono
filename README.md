# docker-mono

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-jackett.svg)](https://bitbucket.org/mreil-com/docker-jackett/addon/pipelines/home)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> [Mono](https://www.mono-project.com/) image

Run .NET Framework applications on Linux.
Based on [mreil/ubuntu-base][ubuntu-base].


## Usage

Use as docker base image.

    FROM mreil/mono


## Build

    ./gradlew build


## Releases

See [hub.docker.com][docker-image-url].


## License

see [LICENSE](LICENSE)


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base
[docker-image-url]: https://hub.docker.com/r/mreil/mono/tags/
